﻿using System;
using SDL2;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Pro.SdlTest
{
    public class Speedometer : GuiValueElement
    {
        public double Rotater { get; set; }
        public double RealRotater { get; set; }

        SDLTexture background;
        SDLTexture arrow;

        SDLColor arrowColor = new SDLColor(0, 0, 255);
        SDLColor guiColor = new SDLColor(255, 0, 0);

        public Speedometer()
        {
            MaxValue = 200;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawMeter(renderer);
            DrawArrow(renderer);
        }

        public void DrawArrow(SDLRenderer renderer)
        {
            DrawSurface arrowSurf = new DrawSurface(20, 100);
            for (int y = 0; y < 100; y++)
            {
                for (int x = 0; x < 20; x++)
                {
                    double relHeight = y / 100.0;
                    if (Math.Abs(x - 10) < relHeight * 10)
                    {
                        arrowSurf[x, y] = arrowColor;
                    }
                }
            }
            arrow = arrowSurf.MakeTexture(renderer);
            arrow.BlendMode = BlendMode.Blend;
        }

        public void DrawMeter(SDLRenderer renderer)
        {
            int width = 451, height = 451;
            int centerX = width / 2, centerY = height / 2;
            int scale = 2;
            DrawSurface backGuiSurf = new DrawSurface(width, height);
            for (int y = 0; y < height; y++)
            {
                int relY = y - centerY;
                for (int x = 0; x < width; x++)
                {
                    int relX = x - centerX;
                    int curRadius = (int)Math.Sqrt(relX * relX + relY * relY);

                    bool steps = curRadius <= 98 * scale && curRadius >= 90 * scale && CarGui.tanPos[x, y] % 10 == 0;
                    bool outerRing = false;// = curRadius == 110 * scale || curRadius == 110 * scale - 1;
                    bool innerOuterRing = curRadius == 100 * scale || curRadius == 100 * scale - 1;
                    bool innerRing = curRadius == 50 * scale || curRadius == 50 * scale - 1;
                    bool degreeCheck = CarGui.tanPos[x, y] < 30 || CarGui.tanPos[x,y] > 150;

                    if ((steps || outerRing || innerOuterRing ) && degreeCheck || innerRing)
                    {
                        backGuiSurf[x, y] = guiColor;
                    }
                }                
            }

            background = backGuiSurf.MakeTexture(renderer);
            background.BlendMode = BlendMode.Blend;

        }

        public override void Update(GameTime gameTime)
        {
            //Rotater += Math.PI / 60;
            //Speed += 1;
            //Speed = Speed % MaxSpeed;
            //Rotater = Rotater  % (240.0 / 360.0 * 2 * Math.PI);
            //Rotater = Rotater  % (240.0 / 360.0 * 2 * Math.PI);
            Rotater = (Persent * (4.0 / 3.0) * Math.PI);
            RealRotater = Rotater - 2.0 / 3.0 * Math.PI;
        }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            //renderer.DrawTexture(background, new SDLRectangle(175, 0, 451, 451));
            renderer.Draw(background, new SDLRectangle(0, 0, 451, 451));
            //renderer.DrawTexture(arrow, new SDLRectangle(0, 0, 20, 100), new SDLRectangle(390, 25, 20, 100), Rotater, new SDLPoint(10, 200), SDL.SDL_RendererFlip.SDL_FLIP_NONE);
            renderer.Draw(arrow, new SDLRectangle(0, 0, 20, 100), new SDLRectangle(215, 25, 20, 100), RealRotater, new SDLPoint(10, 200), RendererFlip.None);
        }
    }
}

