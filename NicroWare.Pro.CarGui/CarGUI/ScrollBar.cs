﻿using System;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Pro.SdlTest
{
    public class ScrollBar : GuiValueElement
    {
        public ScrollBar()
        {
            Size = new SDLPoint(75, 300);
        }

        SDLTexture container;
        SDLTexture cursor;
        bool CapturedMouse = false;

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            
            DrawSurface surf = new DrawSurface(Size.X, Size.Y);
            surf.CustomDraw((x, y) =>
            {
                if(x == 0 || y == 0 || x == Size.X - 1 || y == Size.Y - 1)
                    surf[x,y] = new SDLColor(0,0,255);
            });
            container = surf.MakeTexture(renderer);

            DrawSurface drawCursor = new DrawSurface(Size.X, 25);
            drawCursor.CustomDraw((x, y) =>
            {
                drawCursor[x,y] = new SDLColor(0,255,0); 
            });
            cursor = drawCursor.MakeTexture(renderer);
        }

        public override void Update(GameTime gameTime)
        {
            if (CarGui.mouseDown 
                && CarGui.SideScoll 
                && CarGui.LastMouseX > Location.X 
                && CarGui.LastMouseX < Location.X + Size.X 
                && CarGui.LastMouseY > Location.Y 
                && CarGui.LastMouseY < Location.Y + Size.Y)
            {
                CapturedMouse = true;
                CarGui.SideScoll = false;
            }
            else if (CarGui.mouseDown == false)
                CapturedMouse = false;
            if (CapturedMouse)
            {
                Value = (MaxValue - ((double)(CarGui.y - Location.Y) / (double)Size.Y) * MaxValue);
                /*if (value < 0)
                    value = 0;
                if (value > MaxValue)
                    value = MaxValue;*/
            }
            
        }

        public override void Draw(NicroWare.Lib.SdlEngine.Renderer renderer, GameTime gameTime)
        {
            renderer.Draw(container, new SDLRectangle(0, 0, Size.X, Size.Y));
            renderer.Draw(cursor, new SDLRectangle(0, (int)(Size.Y - cursor.Height - (value / MaxValue * (Size.Y - cursor.Height))), cursor.Width, cursor.Height));
        }
    }
}

