﻿using System;
using SDL2;
using NicroWare.Lib.SdlEngine;
using System.Collections.Generic;

namespace NicroWare.Pro.SdlTest
{
    public class GraphDisplay : GuiElement
    {
        SDLTexture graphTexture;
        SDLTexture overlay;
        List<int> values;
        public int MaxValue { get; set; }

        public GraphDisplay()
        {
            Size = new SDLPoint(1200, 400);
            MaxValue = 200;
            values = new List<int>();
            Console.WriteLine(MaxValue);
            //values.Add(50);
        }

        TimeSpan lastUpdate;

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            graphTexture = new DrawSurface(Size.X, Size.Y).Initialize(s => 
                s.CustomDraw((x, y) =>
                {
                    s[x,y] = new SDLColor(200, 200, 200);
                    if (x == 0 || x == s.Width - 1 || y == 0 || y == s.Height - 1 || x % 20 == 0 || y % 20 == 0)
                        s[x, y] = new SDLColor(150, 150, 150);                        
                })).MakeTexture(renderer);
            overlay = SDLTexture.CreateEmpty(renderer, Size.X, Size.Y, SDL.SDL_PIXELFORMAT_RGBA8888);
            overlay.BlendMode = BlendMode.Blend;
            
        }
        int counter = 0;
        Random rnd = new Random();
        public override void Update(GameTime gameTime)
        {
            if (gameTime.TotalElapsed > lastUpdate + TimeSpan.FromSeconds(0.1))
            {
                values.Add((int)(((Math.Sin(counter / (Math.PI * 5)) + 1) / 2) * (200)));
                counter++;
                int dotSpan = 10;

                lastUpdate = gameTime.TotalElapsed;
                DrawSurface drawer = new DrawSurface(Size.X, Size.Y);
                int listValue = values.Count - Size.X / dotSpan;
                if (listValue < 0)
                    listValue = 0;
                for (int i = 0; i < values.Count && i < Size.X / dotSpan; i++)
                {

                    int realValue = (int)(Size.Y - (values[listValue + i] / (double)MaxValue * ((double)Size.Y - 1)));
                    SDLColor pointColor = new SDLColor((byte)(i * 10 % 256), 0, 0);
                    if (realValue < Size.Y)
                    {
                        drawer[i * dotSpan + 1, realValue] = pointColor;
                        drawer[i * dotSpan - 1, realValue] = pointColor;
                        drawer[i * dotSpan, realValue] = pointColor;
                        if (realValue < Size.Y - 2)
                            drawer[i * dotSpan, realValue + 1] = pointColor;
                        if (realValue > 1)
                            drawer[i * dotSpan, realValue - 1] = pointColor;
                    }
                    if (i > 0 && realValue < Size.Y - 1)
                    {
                        int prevRealValie = (int)(Size.Y - (values[listValue + i - 1] / (double)MaxValue * ((double)Size.Y - 1)));
                        if (prevRealValie < Size.Y - 1)
                        {
                            double claim = (realValue - prevRealValie) / (double)dotSpan;
                            for (int j = 0; j < dotSpan; j++)
                            {
                                drawer[(i - 1) * dotSpan + j, (int)(prevRealValie + (claim * j))] = new SDLColor(0, 0, 0);
                            }
                        }
                    }
                }
                drawer.FillTexture(overlay);
            }
            base.Update(gameTime);
        }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            renderer.Draw(graphTexture, new SDLRectangle(0, 0, Size.X, Size.Y));
            renderer.Draw(overlay, new SDLRectangle(0, 0, Size.X, Size.Y));
        }
    }
}

