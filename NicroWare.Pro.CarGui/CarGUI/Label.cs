﻿using System;
using NicroWare.Lib.SdlEngine;
using SDL2;

namespace NicroWare.Pro.SdlTest
{
    public class Label : GuiElement
    {

        /*SDLSurface preText;
        SDLTexture text;*/
        public bool DynamicSize { get; set; }

        public Label()
        {
            Location = new SDLPoint(0, 0);
            Size = new SDLPoint(100, 40);
            Text = "Label";
            DynamicSize = false;
        }
            

        public override void Update(GameTime gameTime)
        {
            /*if (preText != null)
                preText.Dispose();
            if (preText != null)
                text.Dispose();
            preText = SDLSurface.FromPointer(SDL_ttf.TTF_RenderText_Solid(CarGui.digiFont.BasePointer, Text, new SDL.SDL_Color() { r = 255, g = 255, b = 255}));*/
        }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            //text = SDLTexture.CreateFrom(renderer, preText);
            //renderer.Draw(text, new SDLRectangle(Location.X, Location.Y, /*(int)(Size.Y / 1.5 * Text.Length)*/ Size.X, Size.Y));
            if (!DynamicSize)
            {
                renderer.DrawText(Text, CarGui.digiFont, new SDLRectangle(0, 0, Size.X, Size.Y), new SDLColor(255, 255, 255));
            }
            else
            {
                int width = (int)(Size.Y * 0.6 * Text.Length);
                renderer.DrawText(Text, CarGui.digiFont, new SDLRectangle(0, 0, width, Size.Y), new SDLColor(255, 255, 255));
            }
        }
    }
}

