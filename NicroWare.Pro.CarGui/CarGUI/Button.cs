﻿using System;
using SDL2;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Pro.SdlTest
{
    public class Button : GuiElement
    {
        
        SDLSurface preText;
        SDLTexture text;
        public delegate void ButtonPress(object sender, EventArgs e);
        public event ButtonPress Click;
        public bool DynamicTextSize {get; set;}

        SDLTexture buttonTexture;

        public Button()
        {
            Location = new SDLPoint(0, 0);
            Size = new SDLPoint(200, 70);
            Text = "Hei";
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawSurface surface = new DrawSurface(Size.X, Size.Y);
            for (int y = 0; y < surface.Height; y++)
            {
                for (int x = 0; x < surface.Width; x++)
                {
                    if (x == 0 || y == 0 || x == surface.Width - 1 || y == surface.Height - 1)
                    {
                        surface[x, y] = new SDLColor(250, 250, 250);
                    }
                }
            }
            buttonTexture = surface.MakeTexture(renderer);
            //preText = SDLSurface.FromPointer(SDL_ttf.TTF_RenderText_Solid(CarGui.digiFont.BasePointer, Text, new SDL.SDL_Color() { r = 255, g = 255, b = 255}));
            //text = SDLTexture.CreateFrom(renderer, preText);
        }

        public override void Update(GameTime gameTime)
        {
            if (CarGui.LastMouseX > Location.X && CarGui.LastMouseX < Location.X + Size.X && CarGui.LastMouseY > Location.Y && CarGui.LastMouseY < Location.Y + Size.Y)
            {
                //Location = new SDLPoint(Location.X + 10, Location.Y);
                if (Click != null)
                    Click(this, new EventArgs());
            }
        }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            renderer.Draw(buttonTexture, new SDLRectangle(0, 0, Size.X, Size.Y));
            if (!DynamicTextSize)
            {
                renderer.DrawText(Text, CarGui.digiFont, new SDLRectangle(5, 5, (int)(Size.Y / 1.5 * Text.Length), Size.Y), new SDL.SDL_Color() { r = 255, g = 255, b = 255 });
                //renderer.Draw(text, new SDLRectangle(5, 5, (int)(Size.Y / 1.5 * Text.Length), Size.Y));
            }
            else
            {
                
                int height = (int)(((double)Size.X / (Text.Length)) * 1.3);
                int paddingTop = (int)((Size.Y) / 2.0 - height / 2.0);

                //renderer.Draw(text, new SDLRectangle(5 ,5 + paddingTop, Size.X- 10, height));
                renderer.DrawText(Text, CarGui.digiFont, new SDLRectangle(5, 5 + paddingTop, Size.X- 10, height), new SDL.SDL_Color() { r = 255, g = 255, b = 255 });
            }
        }
    }
}

