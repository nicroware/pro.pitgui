﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections.Generic;
using NicroWare.Lib.Sensors;
using NicroWare.Lib.Networking.Manager;
using NicroWare.Lib.SdlEngine;
using NicroWare.Lib.SdlGui;
using NicroWare.Lib.SdlEngine.Events;
using NicroWare.Pro.SdlTest;

namespace NicroWare.Pro.CarGui
{
    public partial class PitGui : SDLRenderWindow
    {
        SDLRenderer renderer;

        List<GuiPage> guiPages;
        GuiPage activePage;
        int currentPage;
        int CurrentPage
        {
            get
            {
                return currentPage;
            }
            set
            { 
                if (value < 0)
                    currentPage = 0;
                else if (value > guiPages.Count - 1)
                    currentPage = guiPages.Count - 1;
                else
                    currentPage = value;
                activePage = guiPages[currentPage];
            }
        }

        int MaxSpeed = 300;

        //public static bool SideScroll = true;



        SDLTexture batImg;
        SDLTexture rpmImg;




        public PitGui()
        {
            MouseData.LastPos = new SDLPoint(-1, -1);
            Size = new SDLPoint(1920, 1080);
        }

        public override void Initialize()
        {
            //Initial Comment

            renderer = SDLRenderer.Create(this);
            InitializeTan();

            FileInfo textFile = new FileInfo("Content/SFDigital.ttf");
            Global.Font = SDLFont.LoadFont(textFile.FullName, 50);
            guiPages = new List<GuiPage>();

            DataReciver.DataWrapperUpdate += DataReciver_DataWrapperUpdate;

            InitializePage1();
            InitializePage2();
            InitializePage3();

            DataReciver.Initialize();

            //Set current page
            CurrentPage = 0;

            base.Initialize();
        }

        void DataReciver_DataWrapperUpdate (object sender, DataWrapperUpdateEventArgs e)
        {
            SwitchBody<SensorLookup> body = SwitchBody<SensorLookup>.CreateSwitch()
                .Case(SensorLookup.GetByName(SensorLookup.SPEED), () => SpeedGraph.Values.Add(e.Data.Value))
                .Case(SensorLookup.GetByName(SensorLookup.SOC), () => SpeedGraph.Values.Add(e.Data.Value))
                .Case(SensorLookup.GetByName(SensorLookup.CURRENT), () => SpeedGraph.Values.Add(e.Data.Value))
                .Case(SensorLookup.GetByName(SensorLookup.VOLTAGE), () => SpeedGraph.Values.Add(e.Data.Value))
                .Case(SensorLookup.GetByName(SensorLookup.VOLTAGE12), () => SpeedGraph.Values.Add(e.Data.Value))
                .Case(SensorLookup.GetByName(SensorLookup.TEMPBAT), () => SpeedGraph.Values.Add(e.Data.Value))
                .Case(SensorLookup.GetByName(SensorLookup.TEMPCOOL), () => SpeedGraph.Values.Add(e.Data.Value))
                ;

            body.Switch(SensorLookup.GetById(e.Data.SensorID)).Run();
        }

        public void InitializeTan()
        {
            Global.TanPos = new int[451, 451];
            for (int y = 0; y < 451; y++)
            {
                for (int x = 0; x < 451; x++)
                {
                    Global.TanPos[x, y] = (int)(Math.Atan2(y - 225, x - 225) / (Math.PI * 2) * 360 + 0.5 );
                }
            }
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        public override void UnloadContent()
        {
            
        }

        KeyboardState curKey;
        KeyboardState prevKey;
        MouseState prevMouse;
        MouseState curMouse;

        public void DoEvent()
        {
            prevKey = curKey;
            prevMouse = curMouse;

            curKey = Keyboard.GetState();
            curMouse = Mouse.GetState();

            MouseData.LastPos = new SDLPoint(-1, -1);

            if (curKey.IsKeyDown(Keys.UP))
                speedometer.Value += 1;
            else if (curKey.IsKeyDown(Keys.DOWN))
                speedometer.Value -= 1;
            if (curKey.IsKeyDown(Keys.LEFT) && !prevKey.IsKeyDown(Keys.LEFT))
                CurrentPage++;
            else if (curKey.IsKeyDown(Keys.RIGHT) && !prevKey.IsKeyDown(Keys.RIGHT))
                CurrentPage--;

            if (!Input.Initialized)
            {
                if (curMouse.IsKeyDown(MouseButtons.Left))
                {
                    if (!prevMouse.IsKeyDown(MouseButtons.Left))
                    {
                        Global.SideScroll = true;
                        int x, y;
                        MouseData.Pos = curMouse.Location;
                        MouseData.LastPos = curMouse.Location;
                        MouseData.LastDownPos = curMouse.Location;
                        Console.WriteLine("Mouse X: {0}\tY: {1}", MouseData.Pos.X, MouseData.Pos.Y);
                        startLocation = activePage.Location;
                        MouseData.Down = true;
                    }
                    else
                    {
                        MouseData.Pos = curMouse.Location;
                    }
                }
                else if (!curMouse.IsKeyDown(MouseButtons.Left) && prevMouse.IsKeyDown(MouseButtons.Left))
                {
                    if (Global.SideScroll)
                    {
                        CheckShiftPage();
                    }
                    MouseData.Down = false;
                    activePage.Location = new SDLPoint(0, 0);
                }
            }
            


            InputEvent? evn;
            bool setMouse = false;
            while ((evn = Input.GetEvent()) != null)
            {
                InputEvent ev = evn.Value;
                if (ev.type == 3 && !MouseData.Down)
                {
                    Global.SideScroll = true;
                    if (ev.code == 0)
                    {
                        MouseData.Pos = new SDLPoint((int)(ev.value / 4096.0 * Size.X), MouseData.Pos.Y);
                        MouseData.LastPos = MouseData.Pos;
                        MouseData.LastDownPos = MouseData.LastPos;
                    }
                    else if (ev.code == 1)
                    {
                        MouseData.Pos = new SDLPoint( MouseData.Pos.X, (int)(ev.value / 4096.0 * Size.Y));
                        MouseData.LastPos = MouseData.Pos;
                        MouseData.LastDownPos = MouseData.LastPos;
                    }
                    setMouse = true;
                }
                if (ev.type == 3 && MouseData.Down)
                {
                    if (ev.code == 0)
                        MouseData.Pos = new SDLPoint((int)(ev.value / 4096.0 * Size.X), MouseData.Pos.Y);
                    else if (ev.code == 1)
                        MouseData.Pos = new SDLPoint(MouseData.Pos.X, (int)(ev.value / 4096.0 * Size.Y));
                }
                if (ev.type == 1)
                {
                    if (ev.code == 272 && ev.value == 0)
                    {
                        if (MouseData.Down && Global.SideScroll)
                        {
                            CheckShiftPage();
                            activePage.Location = new SDLPoint(0, 0);
                        }
                        MouseData.Down = false;
                    }
                        
                }
            }
            if (setMouse)
                MouseData.Down = true;

        }

        public void CheckShiftPage()
        {
            if (MouseData.Pos.X - MouseData.LastDownPos.X < -100)
                CurrentPage++;
            else if (MouseData.Pos.X - MouseData.LastDownPos.X > 100)
                CurrentPage--;
        }

        public void UpdateValues()
        {
            ushort[] sensors = SensorLookup.SensorIDs;
            DataWrapper? wrapper = null; 
            SwitchBody<SensorLookup> switcher = SwitchBody<SensorLookup>.CreateSwitch()
                .Case(SensorLookup.GetByName(SensorLookup.SPEED), () => speedometer.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SPEED).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SOC), () => batMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SOC).LowLimit)
                /*.Case(SensorLookup.GetByName(SensorLookup.VOLTAGE12), () => voltMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.VOLTAGE12).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPBAT), () => tempBatMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.TEMPBAT).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPCOOL), () => tempCoolMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.TEMPCOOL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL), () => speedPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.BREAKPEDDAL), () => breakPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.BREAKPEDDAL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.DRVWHEEL), () => wheelTurnMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.DRVWHEEL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SUSPENSION), () => suspensionMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SUSPENSION).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.CURRENT), () => currentBarMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.CURRENT).LowLimit)*/
                ;
            
            for (ushort i = 0; i < sensors.Length; i++)
            {
                wrapper = DataReciver.GetData(i);

                if (wrapper != null)
                {
                    switcher.Switch(SensorLookup.GetById(i)).Run();
                }
            }
        }

        double sinCounter = 0;

        public override void Update(GameTime gameTime)
        {
            UpdateValues();
            DoEvent();

            activePage.Update(gameTime);
            if (MouseData.Down && Global.SideScroll)
            {
                if (MouseData.Pos.X > 6 && MouseData.Pos.X < Size.X - 6)
                    activePage.Location = new SDLPoint(startLocation.X + (MouseData.Pos.X - MouseData.LastDownPos.X), 0);
            }
            speedLabel.Text = (speedometer.Persent * MaxSpeed).ToString("000"); 
            /*double part = Math.PI * 2 / allScolls.Length;
            for (int i = 0; i < allScolls.Length; i++)
            {
                allScolls[i].Value = (Math.Sin(sinCounter + part * i) + 1) / 2;
            }
            sinCounter += 0.005;*/

        }

        SDLPoint startLocation;

        public override void Draw(GameTime gameTime)
        {
            renderer.Clear();

            activePage.Draw(renderer, gameTime);

            renderer.Present();
        }
    }
}

/*SDL.SDL_Event e;
            while (SDL.SDL_PollEvent(out e) != 0)
            {
                if (e.type == SDL.SDL_EventType.SDL_QUIT)
                    Quit();
                else if (e.type == SDL.SDL_EventType.SDL_KEYDOWN)
                {
                    /*if (e.button.button == 82) //Up
                        speedometer.Value += 1;
                    if (e.button.button == 81) //Down
                        speedometer.Value -= 1;
                    else if (e.button.button == 79) //Right
                        CurrentPage++;
                    else if (e.button.button == 80) //Left
                        CurrentPage--;
                    else if (e.button.button == 26) //W
                    {
                        batMeter.Value += batMeter.MaxValue / 60;
                        rpmMeter.Value += rpmMeter.MaxValue / 60;
                        otherMeter.Value += otherMeter.MaxValue / 60;
                    }
                    else if (e.button.button == 22) //s
                    {
                        batMeter.Value -= batMeter.MaxValue / 60;
                        rpmMeter.Value -= rpmMeter.MaxValue / 60;
                        otherMeter.Value -= otherMeter.MaxValue / 60;
                    }
                    else if (e.button.button == 4) //A
                    {
                        wheelTurn.Value -= wheelTurn.MaxValue / 60;
                    }
                    else if (e.button.button == 7) //D
                    {
                        wheelTurn.Value += wheelTurn.MaxValue / 60;
                    }
                    else if (e.button.button == 41)
                        Quit();
                }
                if (!Input.Initialized)
                {
                    if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN && !MouseData.Down)
                    {
                        Global.SideScroll = true;
                        int x, y;
                        SDL.SDL_GetMouseState(out x, out y);
                        MouseData.Pos = new SDLPoint(x, y);
                        MouseData.LastPos = new SDLPoint(x, y);
                        MouseData.LastDownPos = new SDLPoint(x, y);
                        startLocation = activePage.Location;
                        MouseData.Down = true;

                    }
                    else if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONUP)
                    {
                        if (MouseData.Down && Global.SideScroll)
                        {
                            CheckShiftPage();
                        }
                        MouseData.Down = false;
                        activePage.Location = new SDLPoint(0, 0);
                    }
                    if (e.type == SDL.SDL_EventType.SDL_MOUSEMOTION)
                    {
                        if (MouseData.Down)
                        {
                            int x, y;
                            SDL.SDL_GetMouseState(out x, out y);
                            MouseData.Pos = new SDLPoint(x, y);
                        }
                    }
                }
            }*/