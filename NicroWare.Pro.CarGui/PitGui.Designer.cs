﻿using System;
using NicroWare.Lib.SdlGui;
using NicroWare.Lib.SdlEngine;
using System.IO;
using NicroWare.Lib.Sensors;

namespace NicroWare.Pro.CarGui
{
    public partial class PitGui
    {
        Speedometer speedometer;
        InfoMeter batMeter;
        InfoMeter rpmMeter;
        InfoMeter otherMeter;
        InfoMeter otherOtherMeter;
        SideMeter wheelTurn;
        Label speedLabel;

        GraphDisplay SpeedGraph;
        GraphDisplay SOCGraph;
        GraphDisplay CurrentGraph;
        GraphDisplay VoltageGraph;
        GraphDisplay Voltage12Graph;
        GraphDisplay TempbatGraph;
        GraphDisplay TempCoolGraph;

        public void InitializePage1()
        {
            FileInfo batPath = new FileInfo("Content/Battery.bmp");
            FileInfo rpmPath = new FileInfo("Content/RPM.bmp");
            batImg = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(batPath.FullName));
            rpmImg = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(rpmPath.FullName));


            speedometer = new Speedometer() 
            { 
                Location = new SDLPoint(175, 15), 
                MaxValue = SensorLookup.GetByName(SensorLookup.SPEED).SpanLimit 
            };
            batMeter = new InfoMeter() { Location = new SDLPoint(550, 35) };
            rpmMeter = new InfoMeter() { Location = new SDLPoint(90, 35), Flip = RendererFlip.Horisontal };
            otherMeter = new InfoMeter() { Location = new SDLPoint(90, 260), Flip = RendererFlip.Both};
            otherOtherMeter = new InfoMeter() { Location = new SDLPoint(550, 260), Flip =  RendererFlip.Vertical};
            wheelTurn = new SideMeter() { Location = new SDLPoint(250, 380) };

            Image batImgBox = new Image() { Location = new SDLPoint(720, 100), ImageTexture = batImg };
            Image rpmImgBox = new Image() { Location = new SDLPoint(48, 100), ImageTexture = rpmImg };

            speedLabel = new Label()
            {
                Location = new SDLPoint(327,200 + 15),
                Size = new SDLPoint(150, 60),
                Text = "888",
                DynamicTextSize = false
            };

            Label hundredLabel = new Label()
            {
                Location = new SDLPoint(700, 25),
                Size = new SDLPoint(18 * 3, 25),
                Text = "100",
            };
            Label zeroLabel = new Label()
            {
                Location = new SDLPoint(700, 205),
                Size = new SDLPoint(18 * 2, 25),
                Text = " 0",
            };
            Label hundredLabel2 = new Label()
            {
                Location = new SDLPoint(-6, 25),
                Size = new SDLPoint(18 * 5, 25),
                Text = "10000",
            };
            Label zeroLabel2 = new Label()
            {
                Location = new SDLPoint(30, 205),
                Size = new SDLPoint(18 * 3, 25),
                Text = "  0",
            };
            Button button1 = new Button() { Text = "Next", Location = new SDLPoint(570, 380)};;
            button1.Click += (object sender, EventArgs e) => CurrentPage++;
            GuiPage page1 = new GuiPage(renderer);
           


            SpeedGraph = new GraphDisplay()
            {
                Location = new SDLPoint(45, 500),
                Size = new SDLPoint(720, 400),
                MaxValue = SensorLookup.GetByName(SensorLookup.SPEED).SpanLimit 
            };
            TextBlock speedGraphText = new TextBlock()
            {
                Location = SpeedGraph.Location + new SDLPoint(0, -30),
                Size = new SDLPoint(SpeedGraph.Size.X, 0),
                Text = "Speed",
                FontSize = 30,
                TextAlign = Align.Center,
            };
            Label speedGraphMaxLabel = new Label()
            {
                Location = SpeedGraph.Location + new SDLPoint(-40, 0),
                Size = new SDLPoint(0, 20),
                Text = "300",
            };
            Label speedGraphMinLabel = new Label()
            { 
                Location = SpeedGraph.Location + new SDLPoint(-40, 385),
                Size = new SDLPoint(0, 20),
                Text = "0",
            };
  
            SOCGraph = new GraphDisplay()
            {
                Location = new SDLPoint(800, 30),
            };
            TextBlock SOCGraphText = new TextBlock()
            {
                Location = SOCGraph.Location + new SDLPoint(0, -30),
                Size = new SDLPoint(SOCGraph.Size.X, 0),
                Text = "State Of Charge",
                FontSize = 30,
                TextAlign = Align.Center,
            };
            Label SOCGraphMaxLabel = new Label()
            {
                    Location = SOCGraph.Location + new SDLPoint(-40, 0),
                    Size = new SDLPoint(0, 20),
                    Text = "100",
            };
            Label SOCGraphMinLabel = new Label()
            {
                Location = SOCGraph.Location + new SDLPoint(-40, 285),
                Size = new SDLPoint(0, 20),
                Text = "  0",
            };
            
            CurrentGraph = new GraphDisplay()
            {
                Location = new SDLPoint(1380, 30),

            };
            TextBlock CurrentGraphText = new TextBlock()
            {
                Location = CurrentGraph.Location + new SDLPoint(0, -30),
                Size = new SDLPoint(CurrentGraph.Size.X, 0),
                Text = "Current",
                FontSize = 30,
                TextAlign = Align.Center,
            };
            Label CurrentGraphMaxLabel = new Label()
            {
                Location = CurrentGraph.Location + new SDLPoint(-40, 0),
                Size = new SDLPoint(0, 20),
                Text = "100",
            };
            Label CurrentGraphMinLabel = new Label()
            {
                Location = CurrentGraph.Location + new SDLPoint(-40, 285),
                Size = new SDLPoint(0, 20),
                Text = "  0",
            };

            VoltageGraph = new GraphDisplay()
            {
                Location = new SDLPoint(800, 360),

            };
            TextBlock VoltageGraphText = new TextBlock()
            {
                Location = VoltageGraph.Location + new SDLPoint(0, -30),
                Size = new SDLPoint(VoltageGraph.Size.X, 0),
                Text = "Battery Voltage",
                FontSize = 30,
                TextAlign = Align.Center,
            };
            Label VoltGraphMaxLabel = new Label()
            {
                Location = VoltageGraph.Location + new SDLPoint(-40, 0),
                Size = new SDLPoint(0, 20),
                Text = "100",
            };
            Label VoltGraphMinLabel = new Label()
            {
                Location = VoltageGraph.Location + new SDLPoint(-40, 285),
                Size = new SDLPoint(0, 20),
                Text = "  0",
            };

            Voltage12Graph = new GraphDisplay()
            {
                Location = new SDLPoint(1380, 360),

            };
            TextBlock Voltage12GraphText = new TextBlock()
            {
                Location = Voltage12Graph.Location + new SDLPoint(0, -30),
                Size = new SDLPoint(Voltage12Graph.Size.X, 0),
                Text = "12V Battery Voltage",
                FontSize = 30,
                TextAlign = Align.Center,
            };
            Label Volt12GraphMaxLabel = new Label()
            {
                Location = Voltage12Graph.Location + new SDLPoint(-40, 0),
                Size = new SDLPoint(0, 20),
                Text = "100",
            };
            Label Volt12GraphMinLabel = new Label()
            {
                Location = Voltage12Graph.Location + new SDLPoint(-40, 285),
                Size = new SDLPoint(0, 20),
                Text = "  0",
            };
            
            TempbatGraph = new GraphDisplay()
            {
                Location = new SDLPoint(800, 700),

            };
            TextBlock TempbatGraphText = new TextBlock()
            {
                Location = TempbatGraph.Location + new SDLPoint(0, -30),
                Size = new SDLPoint(TempbatGraph.Size.X, 0),
                Text = "Battery Temperature",
                FontSize = 30,
                TextAlign = Align.Center,
            };
            Label TmpBatGraphMaxLabel = new Label()
            {
                Location = TempbatGraph.Location + new SDLPoint(-40, 0),
                Size = new SDLPoint(0, 20),
                Text = "100",
            };
            Label TmpBatGraphMinLabel = new Label()
            {
                Location = TempbatGraph.Location + new SDLPoint(-40, 285),
                Size = new SDLPoint(0, 20),
                Text = "  0",
            };
            
            TempCoolGraph = new GraphDisplay()
            {
                Location = new SDLPoint(1380, 700),

            };
            TextBlock TempCoolGraphText = new TextBlock()
            {
                Location = TempCoolGraph.Location + new SDLPoint(0, -30),
                Size = new SDLPoint(TempCoolGraph.Size.X, 0),
                Text = "Coolant Temperature",
                FontSize = 30,
                TextAlign = Align.Center,
            };
            Label TmpCoolGraphMaxLabel = new Label()
            {
                Location = TempCoolGraph.Location + new SDLPoint(-40, 0),
                Size = new SDLPoint(0, 20),
                Text = "100",
            };
            Label TmpCoolGraphMinLabel = new Label()
            {
                Location = TempCoolGraph.Location + new SDLPoint(-40, 285),
                Size = new SDLPoint(0, 20),
                Text = "  0",
            };



            /*            GraphDisplay SpeedGraph;
            GraphDisplay SOCGraph;
            GraphDisplay CurrentGraph;
            GraphDisplay VoltageGraph;
            GraphDisplay Voltage12Graph;
            GraphDisplay TempbatGraph;
            GraphDisplay TempCoolGraph;*/

            //Add elements
            page1.Elements.Add(batMeter);
            page1.Elements.Add(rpmMeter);
            page1.Elements.Add(otherMeter);
            page1.Elements.Add(otherOtherMeter);
            page1.Elements.Add(speedometer);
            page1.Elements.Add(wheelTurn);
            page1.Elements.Add(speedLabel);
            page1.Elements.Add(hundredLabel);
            page1.Elements.Add(zeroLabel);
            page1.Elements.Add(hundredLabel2);
            page1.Elements.Add(zeroLabel2);
            page1.Elements.Add(batImgBox);
            page1.Elements.Add(rpmImgBox);
            page1.Elements.Add(SpeedGraph);
            page1.Elements.Add(SOCGraph);
            page1.Elements.Add(CurrentGraph);
            page1.Elements.Add(VoltageGraph);
            page1.Elements.Add(Voltage12Graph);
            page1.Elements.Add(TempbatGraph);
            page1.Elements.Add(TempCoolGraph);
            page1.Elements.Add(speedGraphText);
            page1.Elements.Add(SOCGraphText);
            page1.Elements.Add(CurrentGraphText);
            page1.Elements.Add(VoltageGraphText);
            page1.Elements.Add(Voltage12GraphText);
            page1.Elements.Add(TempbatGraphText);
            page1.Elements.Add(TempCoolGraphText);

            page1.Elements.Add(speedGraphMaxLabel);
            page1.Elements.Add(speedGraphMinLabel);

            page1.Elements.Add(SOCGraphMaxLabel);
            page1.Elements.Add(SOCGraphMinLabel);

            page1.Elements.Add(CurrentGraphMaxLabel);
            page1.Elements.Add(CurrentGraphMinLabel);

            page1.Elements.Add(VoltGraphMaxLabel);
            page1.Elements.Add(VoltGraphMinLabel);

            page1.Elements.Add(Volt12GraphMaxLabel);
            page1.Elements.Add(Volt12GraphMinLabel);

            page1.Elements.Add(TmpBatGraphMaxLabel);
            page1.Elements.Add(TmpBatGraphMinLabel);

            page1.Elements.Add(TmpCoolGraphMaxLabel);
            page1.Elements.Add(TmpCoolGraphMinLabel);
            //page1.Elements.Add(block);
            //page1.Elements.Add(button1);

            //Add page
            guiPages.Add(page1);
            page1.Initialize(renderer);
        }

        ScrollBar[] allScolls;

        public void InitializePage2()
        {
            GuiPage page2 = new GuiPage(renderer);

            Button button2 = new Button() { Text = "Prev", Location = new SDLPoint(30, 380) };
            Button exit = new Button() { Text = "Exit", Location = new SDLPoint(570, 380) };
            ScrollBar scrollBar = new ScrollBar() { Location = new SDLPoint(20, 20) };
            ScrollBar scrollBar1 = new ScrollBar() { Location = new SDLPoint(120, 20) };
            ScrollBar scrollBar2 = new ScrollBar() { Location = new SDLPoint(220, 20) };
            ScrollBar scrollBar3 = new ScrollBar() { Location = new SDLPoint(320, 20) };
            ScrollBar scrollBar4 = new ScrollBar() { Location = new SDLPoint(420, 20) };
            ScrollBar scrollBar5 = new ScrollBar() { Location = new SDLPoint(520, 20) };
            ScrollBar scrollBar6 = new ScrollBar() { Location = new SDLPoint(620, 20) };
            ScrollBar scrollBar7 = new ScrollBar() { Location = new SDLPoint(720, 20) };
            button2.Click += (object sender, EventArgs e) => CurrentPage--;
            exit.Click += (object sender, EventArgs e) => this.Quit();
            page2.Elements.Add(button2);
            page2.Elements.Add(exit);
            page2.Elements.Add(scrollBar);
            page2.Elements.Add(scrollBar1);
            page2.Elements.Add(scrollBar2);
            page2.Elements.Add(scrollBar3);
            page2.Elements.Add(scrollBar4);
            page2.Elements.Add(scrollBar5);
            page2.Elements.Add(scrollBar6);
            page2.Elements.Add(scrollBar7);
            allScolls = new ScrollBar[] { scrollBar, scrollBar1, scrollBar2, scrollBar3, scrollBar4, scrollBar5, scrollBar6, scrollBar7 };
            guiPages.Add(page2);
            page2.Initialize(renderer);
        }

        public void InitializePage3()
        {
            Label hundredLabel = new Label()
            {
                Location = new SDLPoint(20, 20),
                Size = new SDLPoint(0, 30),
                DynamicTextSize = true,
                Text = "Calibration page",
            };
            Button steering = new Button() { Text = "Steering", Location = new SDLPoint(30, 100), DynamicTextSize=true };
            steering.Click += (object sender, EventArgs e) => {
                activePage = new GuiPage(renderer);
                activePage.Elements.Add(new Button() { Location = new SDLPoint(10,10), Text = "Hello World", DynamicTextSize = true});
                Button backButton = new Button() { Location = new SDLPoint(10,110), Text = "Back", DynamicTextSize = true};
                backButton.Click += (object sender2, EventArgs e2) => CurrentPage = CurrentPage;
                activePage.Elements.Add(backButton);
                activePage.Initialize(renderer);
            };

            Button pedals = new Button() { Text = "Pedals", Location = new SDLPoint(30, 200), DynamicTextSize=true };
            pedals.Click += (object sender, EventArgs e) => {
                activePage = new GuiPage(renderer);

                /* Set torque max button and label */
                activePage.Elements.Add( new Label() { 
                    Location = new SDLPoint(400-(384/2), 10), // (20 * 0.6 * 32) => (Size y * 0.6 * Text width)
                    Text = "Press torque pedal in completely", // 32
                    Size = new SDLPoint(0, 20),
                    DynamicTextSize = true 
                } );
                Button tqMaxButton = new Button() { 
                    Location = new SDLPoint(300, 40), 
                    Text = "Set Torque Max", 
                    DynamicTextSize = true 
                };
                tqMaxButton.Click += (object sender2, EventArgs e2) => {
                    Console.WriteLine("Torque Max OK");
                    tqMaxButton.Text = "Torque Max OK";
                    tqMaxButton.FrameColor = SDLColor.Green;
                };

                /* Set brake pedal max button and label */
                activePage.Elements.Add( new Label() {
                    Location = new SDLPoint(400-(383/2), 140),
                    Text = "Press brake pedal in completely",
                    Size = new SDLPoint(0, 20),
                    DynamicTextSize = true 
                } );
                Button brkMaxButton = new Button() {
                    Location = new SDLPoint(300, 170), 
                    Text = "Set Brake Max", 
                    DynamicTextSize = true 
                };
                brkMaxButton.Click += (object sender2, EventArgs e2) => {
                    Console.WriteLine("Brake Max OK");
                    brkMaxButton.Text = "Brake Max OK";
                };

                /* Set torque and brake pedal min button and label */
                activePage.Elements.Add( new Label() {
                    Location = new SDLPoint(400-(228/2), 270),
                    Text = "Release both pedals", // 28
                    Size = new SDLPoint(0, 20),
                    DynamicTextSize = true 
                } );
                Button setMinButton = new Button() {
                    Location = new SDLPoint(300, 300), 
                    Text = "Set Pedal Min", 
                    DynamicTextSize = true 
                };
                setMinButton.Click += (object sender2, EventArgs e2) => {
                    Console.WriteLine("Pedal Min OK");
                    setMinButton.Text = "Pedal Min OK";
                };

                Button backButton = new Button() { Location = new SDLPoint(10, 400), Text = "Back", DynamicTextSize = true };
                backButton.Click += (object sender2, EventArgs e2) => CurrentPage = CurrentPage;

                activePage.Elements.Add(backButton);
                activePage.Elements.Add(tqMaxButton);
                activePage.Elements.Add(brkMaxButton);
                activePage.Elements.Add(setMinButton);
                activePage.Initialize(renderer);

            };

            GuiPage calibrationPage = new GuiPage(renderer);
            calibrationPage.Elements.Add(hundredLabel);
            guiPages.Add(calibrationPage);
            calibrationPage.Elements.Add(pedals);
            calibrationPage.Elements.Add(steering);

            calibrationPage.Initialize(renderer);
        }
    }
}

