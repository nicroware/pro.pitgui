﻿using System;
using System.Collections.Generic;
using NicroWare.Lib.Networking.Manager;
using NicroWare.Lib.Networking.Logger;
using NicroWare.Lib.Sensors;
using NicroWare.Lib.SdlEngine;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO.Ports;
using System.IO;


namespace NicroWare.Pro.SdlTest
{

    public class DataWrapperUpdateEventArgs : EventArgs
    {
        public DataWrapper Data { get; private set; }
        public DataWrapperUpdateEventArgs(DataWrapper wrapper)
        {
            this.Data = wrapper;
        }
    }

    public delegate void DataWrapperEvent(object sender, DataWrapperUpdateEventArgs e);

    public class DataReciver
    {
        public static event DataWrapperEvent DataWrapperUpdate;
        public DataReciver()
        {
            
        }

        static LogManager manager;
        static LogInterface receiveLogger;
        static LogInterface transmitLogger;

        static Dictionary<SensorLookup, DataWrapper> lastesTable = new Dictionary<SensorLookup, DataWrapper>(); 

        public static DataWrapper? GetData(ushort sensorID)
        {
            return GetData(SensorLookup.GetById(sensorID));
        }

        public static DataWrapper? GetData(string name)
        {
            return GetData(SensorLookup.GetByName(name));
        }

        public static DataWrapper? GetData(SensorLookup lookup)
        {
            if (lookup == null)
                return null;
            if (lastesTable.ContainsKey(lookup))
                return lastesTable[lookup];
            return null;
        }

        static SerialPort xbeePort;
        //static SerialPort piPort;

        public static void Initialize()
        {
            manager = new LogManager();
            manager.OnVerbose += (object sender, LogWriteEventArgs e) => Console.WriteLine("(" + e.Entry.Time.ToString("HH:mm:ss") +  ")[" + e.Entry.Sender + "]\t" + e.Entry.Value);
            manager.OnWarning += (object sender, LogWriteEventArgs e) => 
            {
                if (e.Entry is ExceptionEntry)
                {
                    Console.WriteLine((e.Entry as ExceptionEntry).Exception);
                }
            };
            receiveLogger = manager.CreateLogger("RECEIVER");
            transmitLogger = manager.CreateLogger("TRANSMIT");
            //DataWrapperUpdate += SendToWeb;
            //TODO: Uncomment to send to web 
            try
            {
                xbeePort = new SerialPort("/dev/ttyUSB0", 19200, Parity.None, 8, StopBits.One);
                xbeePort.Open();
                //piPort = new SerialPort("/dev/ttyAMA0", 115200, Parity.None, 8, StopBits.One);
                //piPort.Open();

                Task.Run(ReceiveLoop);

                //Task.Run(SendLoop);
            }
            catch(Exception e)
            {
                receiveLogger.WriteException("Error oppening connection", e, 4);
            }
            Task.Run(new Action(SendToWeb2));
        }

        public static void ReceiveLoop()
        {
            receiveLogger.WriteLine("Started listening", "INFO", 1);
            UdpNetworkClient client = new UdpNetworkClient(xbeePort, 151);
            receiveLogger.WriteLine("Open com port", "INFO", 1);
            int packagesReceived = 0;
            try 
            {
                while (running)
                {
                    xbeePort.Close();
                    xbeePort.Open();
                    byte[] bytes = client.Receive();
                    packagesReceived++;
                    receiveLogger.WriteLine("Recved message, bytes left to read: " + xbeePort.BytesToRead + " package count: " + packagesReceived, "INFO", 1);
                    for (int i = 0; i < bytes.Length; i += 3)
                    {
                        AddValue(DataWrapper.ReadData(bytes, i, true));
                    }
                }
            }
            catch(Exception e)
            {
                receiveLogger.WriteException("There has been an error in the recive loop", e, 4);
            }
        }


        public static bool running = true;

        public static void SendLoop()
        {
            transmitLogger.WriteLine("Creating trasmit client", "INFO", 1);
            UdpNetworkClient client = new UdpNetworkClient(xbeePort, 150);
            client.Connect(151);
            int counter = 0;
            transmitLogger.WriteLine("Startet trasmit loop", "INFO", 1);

            while (xbeePort.CtsHolding && running)
            {
                //byte[] tempArray = new byte[3 * (counter % 5 == 0 ? 8 : 2)];
                List<byte> bytes = new List<byte>();
                for(ushort i = 0; i < (counter % 5 == 0 ? 8 : 2);i++)
                {
                    DataWrapper? wrapper = GetData(i);
                    if (wrapper != null)
                    {
                        bytes.AddRange(wrapper.Value.GetBytes(true));
                    }
                }
                try
                {
                    client.Send(bytes.ToArray(), true);
                    transmitLogger.WriteLine("Sent message nr: " + counter, "INFO", 1);
                }
                catch(Exception e)
                {
                    transmitLogger.WriteException("There has been an exception in the send loop", e, 4);
                }
                counter++;
            }
        }



        private static void AddValue(DataWrapper wrapper)
        {
            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (lastesTable.ContainsKey(lookup))
                lastesTable[lookup] = wrapper;
            else
                lastesTable.Add(lookup, wrapper);
            if (DataWrapperUpdate != null)
                DataWrapperUpdate(null, new DataWrapperUpdateEventArgs(wrapper));
        }

        public static void SendToWeb(object sender, DataWrapperUpdateEventArgs e)
        {
            //http://ionracing.no/sensor/reg.php
            WebClient client = new WebClient();
            string s = client.DownloadString("http://ionracing.no/sensor/reg.php?");
            Console.WriteLine(s);
            //System.Threading.Thread
            /*
             * User-Agent:   Mozilla/5.0 (Windows NT 6.3; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0
             * Host:   www.piuha.fi
             * Connection: keep-alive
             * Accept-Language:    en-US,en;q=0.5
             * Accept-Encoding:    gzip, deflate
             * Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*//*;q=0.8*/
        }


        public static void SendToWeb2()
        {
            //http://ionracing.no/sensor/reg.php
            WebClient client = new WebClient();
            string latitude = "";
            string longitude = "";

            double deg = SensorLookup.GetByName(SensorLookup.GPSDEG).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value)));
            double min = SensorLookup.GetByName(SensorLookup.GPSMIN).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value)));
            double secLat = SensorLookup.GetByName(SensorLookup.LAT).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value)));
            double secLong = SensorLookup.GetByName(SensorLookup.LONG).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value)));

            /*deg = 29701;
            min = 3625;
            secLat = 28735;
            secLong = 46132;*/


            latitude = string.Format("{0}{1} {2}.{3}", (((int)min >> 15 & 1) == 0 ? "N" : "S") ,((int)deg >> 9).ToString(), ((int)min >> 6 & 63), secLat);
            longitude = string.Format("{0}{1} {2}.{3}", (((int)min >> 14 & 1) == 0 ? "E" : "W") ,((int)deg & 1023).ToString(), ((int)min & 63), secLong);


            string queryString = string.Format("http://ionracing.no/sensor/reg.php?s={0}&soc={1}&cm={2}&vm={3}&vm2={4}&bt={5}&ct={6}&lat={7}&long={8}"
                , SensorLookup.GetByName(SensorLookup.SPEED).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value))).ToString()
                , SensorLookup.GetByName(SensorLookup.SOC).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value))).ToString()
                , SensorLookup.GetByName(SensorLookup.CURRENT).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value))).ToString()
                , SensorLookup.GetByName(SensorLookup.VOLTAGE).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value))).ToString()
                , SensorLookup.GetByName(SensorLookup.VOLTAGE12).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value))).ToString()
                , SensorLookup.GetByName(SensorLookup.TEMPBAT).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value))).ToString()
                , SensorLookup.GetByName(SensorLookup.TEMPCOOL).Use(x => x.ConvertValue(GetData(x.ID).IfNotNull(0, y => y.Value.Value))).ToString()
                , latitude
                , longitude);
            string s = client.DownloadString(queryString);
            Console.WriteLine(s);
            System.Threading.Thread.Sleep(1000);
            /*
             * User-Agent:   Mozilla/5.0 (Windows NT 6.3; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0
             * Host:   www.piuha.fi
             * Connection: keep-alive
             * Accept-Language:    en-US,en;q=0.5
             * Accept-Encoding:    gzip, deflate
             * Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*//*;q=0.8*/

            /*Fart - s (decimal(5, 2))
            State of charge (Hvor stor ladning er igjen) - soc (decimal(5, 2))
            Strøm måler - cm (decimal(5, 2))
            Spennings måler - vm (decimal(5, 2))
            12 V spennings måler, for det andre batteriet - vm2 (decimal(5, 2))
            Batteri temperatur - bt (decimal(5, 2))
            Kjølevekse temperatur - ct (decimal(5, 2))
            latitude - lat (varchar)
            longitude - long (varchar)
            */
        }

        /*public static void ReciveLoop()
        {
            logger.WriteLine("Started listening", "INFO", 1);
            SerialPort serialPort = new SerialPort("/dev/ttyUSB0", 19200, Parity.None, 8, StopBits.One);

            //serialPort.Open();
            logger.WriteLine("Open com port", "INFO", 1);
            //UdpClient client = new UdpClient(1511);
            int packagesRecived = 0;
            while (true)
            {
                serialPort.Close();
                serialPort.Open();
                UDPStreamClient streamClient = new UDPStreamClient(serialPort.BaseStream, 151, manager.CreateLogger("UDP CLIENT"));
                byte[] bytes = streamClient.Recieve();
                packagesRecived++;
                logger.WriteLine("Recved message, bytes left to read: " + serialPort.BytesToRead + " package count: " + packagesRecived, "INFO", 1);
                //IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
                //DataWrapper wrapper = DataWrapper.ReadData(client.Receive(ref endPoint));
                //Console.WriteLine("Recived Message, bytes left to read: " + serialPort.BytesToRead);
                //DataWrapper wrapper = DataWrapper.ReadData(streamClient.Recieve());

                for (int i = 0; i < bytes.Length; i += 6)
                {
                    AddValue(NicroWare.Lib.Networking.DataWrapper.ReadData(bytes, i, true));
                }
            }
            //InternalSim();
        }


        private static void InternalSim()
        {
            bool running = true;
            //UdpClient client = new UdpClient(1510);
            //client.Connect(IPAddress.Loopback, 1511);
            ushort[] sensorIDs = SensorLookup.SensorIDs;
            //Random rnd = new Random();
            Dictionary<ushort, OBJWrap<DataWrapper>> generatedData = new Dictionary<ushort, OBJWrap<DataWrapper>>();
            double counter = 0;
            while (running)
            {
                for (ushort i = 0; i < sensorIDs.Length; i++)
                {
                    if (!generatedData.ContainsKey(i))
                        generatedData.Add(i, new OBJWrap<DataWrapper>(new DataWrapper() { SensorID = i, Value = 0 }));
                    OBJWrap<DataWrapper> wrap = generatedData[i];
                    DataWrapper wrapper = generatedData[i].obj;
                    wrapper.Value = (int)Math.Abs(Math.Sin(counter) * 200);
                    //wrapper.Value += 1;//rnd.Next(0, 4); //rnd.Next(0, 6) - 2;
                    wrapper.Value = wrapper.Value % 200;
                    wrap.obj = wrapper;
                    SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
                    if (lastesTable.ContainsKey(lookup))
                        lastesTable[lookup] = wrapper;
                    else
                        lastesTable.Add(lookup, wrapper);
                    /*byte[] tempArray = wrap.obj.GetBytes();
                    try
                    {
                        client.Send(tempArray, tempArray.Length);
                    }
                    catch
                    {
                        running = false;
                    }

                }
                System.Threading.Thread.Sleep(5);
                counter += 0.005;
            }
        }*/
    }

    public static class Extensions
    {
        public static TRet Use<T, TRet>(this T t, Func<T, TRet> func)
        {
            return func(t);
        }

        public static TRet IfNotNull<T, TRet>(this T t, TRet ifNull, Func<T, TRet> func)
        {
            if (t != null)
                return func(t);
            return ifNull;
        }
    }

    public class OBJWrap<T>
    {
        public T obj { get; set; }

        public OBJWrap(T t)
        {
            this.obj = t;
        }
    }
}

